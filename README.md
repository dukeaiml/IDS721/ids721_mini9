# IDS 721 Mini 9

URL to the app : https://ids721mini9-wbpqvdmfhlxgtswhoa4gss.streamlit.app/


### Dependencies

`streamlit`

`pytorch`

`transformers`

`numpy`

Install the necessary libraries:


```bash
pip install pytorch streamlit transformers

```


### App

Create the llm app:


```python
import streamlit as st
import pandas as pd
import numpy as np
from transformers import pipeline
model = pipeline("text-generation", model="openai-gpt")

def streamInterface():

    st.title('LLM app')
    text = st.text_area("Enter text to ask chatbot:")

     
    if st.button("Generate"):

        if text:
            content = model(text, max_length=100, num_return_sequences=1)[0]['generated_text']
            st.write("Answer:")
            st.write(content)
        else:
            st.warning("Please enter text to ask chatbot.")
            
if __name__ == "__main__":  
    streamInterface()
    

```

### Run 

```bash
streamlit run llmapp.py
```

### Deploy

1. From your workspace at share.streamlit.io, click "New app" from the upper-right corner of your workspace.

![Alt text](image.png)

2. Fill in your repo, branch, and file path. As a shortcut, you can also click "Paste GitHub URL" to paste a link directly to your_app.py on GitHub.

![Alt text](image-1.png)


![Alt text](image-2.png)